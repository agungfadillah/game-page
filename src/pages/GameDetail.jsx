import { Container, Row, Col, Nav, NavItem } from "react-bootstrap"
import '../assets/Games.css'
import { useEffect, useState } from "react"

export default function GameDetail(){
    const [com, setCom] = useState('')
    const [player, setPlayer] = useState('')
    const [result, setResult] = useState('')

    
    const onClick = (e) => {
        setPlayer(e.target.id)
        }
    const randomCom = () => {
        const bot = ['batu', 'gunting', 'kertas']
        setCom(bot[Math.floor(Math.random() * 3)])
    }


    useEffect(() => {
        randomCom();
        if (com===0) {
        // com = "batu";
        document.getElementById("batu2").style.background=
        "#C4C4C4";
        document.getElementById("gunting2").style.background=
        "#9C835F";
        document.getElementById("kertas2").style.background=
        "#9C835F";
        }
        else if (com===1) {
        // com="gunting";
        document.getElementById("gunting2").style.background=
        "#C4C4C4";
        document.getElementById("kertas2").style.background=
        "#9C835F";
        document.getElementById("batu2").style.background=
        "#9C835F";
        } else {
        // com="kertas";
        document.getElementById("kertas2").style.background=
        "#C4C4C4";
        document.getElementById("gunting2").style.background=
        "#9C835F";
        document.getElementById("batu2").style.background=
        "#9C835F";
        }
    }, []);

const batu = () => {
    document.getElementById("batu2").style.background=
        "#C4C4C4";
    document.getElementById("gunting2").style.background=
        "#9C835F";
    document.getElementById("kertas2").style.background=
        "#9C835F";
    // player="batu";
    randomCom();
    winHasil();
    matchResult();
}

const gunting = () => {
    document.getElementById("gunting2").style.background=
    "#C4C4C4";
    document.getElementById("kertas2").style.background=
    "#9C835F";
    document.getElementById("batu2").style.background=
    "#9C835F";
    // player="gunting";
    randomCom();
    winHasil();
    matchResult();
}

const kertas = () => {
    document.getElementById("kertas2").style.background=
    "#C4C4C4";
    document.getElementById("gunting2").style.background=
    "#9C835F";
    document.getElementById("batu2").style.background=
    "#9C835F";
    // player="kertas";
    randomCom();
    winHasil();
    matchResult();
}


const winHasil = () => {
    if ( setPlayer === setCom){
        setResult('Draw')
    }else if( setPlayer === 'batu' && setCom === 'gunting'){
        setResult('Player WIN')
    }else if( setPlayer === 'gunting' && setCom === 'kertas'){
        setResult('Player WIN')
    }else if( setPlayer === 'kertas' && setCom === 'batu'){
        setResult('Player WIN')
    }else{
        setResult('COM WIN')
    }
}
    

const matchResult = () => {
        if(setResult === "Draw"){
            let replace = document.querySelector(".replace")
            replace.innerHTML="DRAW"
            replace.classList.add("result")
            replace.style.background ="#035B0C"
            // if (replace.classList.contain("versus")){ 
            //     replace.classList.remove("versus")
            // }
            console.log("Match Draw")
      } else if(setResult === "Player WIN"){
            let replace = document.querySelector(".replace")
            replace.innerHTML="PLAYER 1 <br> WIN"
            replace.classList.add("result")
            replace.style.background = "#4C9654"
            // if (replace.classList.contain("versus")){
            //     replace.classList.remove("versus")
            // }
            console.log("Player Win")
      } else {
            let replace = document.querySelector(".replace")
            replace.innerHTML="COM <br> WIN"
            replace.classList.add("result")
            replace.style.background = "#4C9654"
            // if (replace.classList.contain("versus")){
            //     replace.classList.remove("versus")
            // }
            console.log("Com Win")
        }
    }



    

    
    return (
        <>
        <div className="body">
            <Container className="d-flex ms-2">
                <Nav>
                    <NavItem className="games p-4 ms-2" id="back">
                        <a href="/home">
                            <img src="/back.png" alt=""/></a>
                    </NavItem>
                    <NavItem className="logo p-2 ms-2" id="logo">
                            <img src="/logo 1.png" alt=""/>
                    </NavItem>  
                    <NavItem className="headers p-4 ms-2">
                            <h1>ROCK PAPER SCISSORS</h1>
                    </NavItem>
                </Nav>
            </Container>
            <Container className="container text-center">
                <Row className="user">
                    <Col className="player p-5 d-flex left">Player 1</Col>
                    <Col className="d-flex center">
                    <div className="com p-5 d-flex right">COM</div>
                    </Col>
                </Row>
                <Row className="row batu">
                    <Col className="col-4 d-flex left">
                        <div className="option" onClick={batu} id="batu">
                        <img src="/batu.png" alt=""/></div>
                    </Col>
                        <Col className="col-4"></Col>
                    <Col className="col-4 d-flex right">
                        <div className="random" id="batu2">
                        <img src="/batu.png" alt=""/></div>
                    </Col>
                </Row>
                <Row className="row kertas">
                    <Col className="col-4 d-flex left">
                        <div className="option" onClick={kertas} id="kertas">
                        <img src="/kertas.png" alt=""/></div>
                    </Col>
                        <Col className="col-4 d-flex versus">
                            <div class="replace d-flex">VS</div>
                        </Col>
                    <Col className="col-4 d-flex right">
                        <div className="random"  id="kertas2">
                        <img src="/kertas.png" alt=""/></div>
                    </Col>
                </Row>
                <Row className="row gunting">
                    <Col className="col-4 d-flex left">
                        <div className="option" onClick={gunting} id="gunting">
                        <img src="/gunting.png" alt=""/></div>
                    </Col>
                        <Col className="col-4"></Col>
                    <Col className="col-4 d-flex right">
                        <div className="random"  id="gunting2">
                        <img src="/gunting.png" alt=""/></div>
                    </Col>
                </Row>

                            <div className="refresh">
                                <div className="col-md-6 offset-md-3">
                                <a href="/game-detail">
                                <img src="/refresh.png" alt="" className="refresh-img"/>
                            </a>
                            </div>
                        </div>
            </Container>
        </div>
        </>
        )
    }

    
    

